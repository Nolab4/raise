﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Raise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Utility
{
    public static class Helper
    {
        public static string admin = "Admin";
        public static string leader = "Leader";
        public static string member = "Member";

        //Helper function for dropdown display
        public static List<SelectListItem> GetRoleForDrowDown()
        {
            return new List<SelectListItem>
            {
                new SelectListItem{Value=Helper.leader,Text=Helper.leader },
                new SelectListItem { Value = Helper.member, Text = Helper.member }
            };
        }
    }

}
