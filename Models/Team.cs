﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Models
{
    public class Team
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Description of the project")]
        public string ProjectDescription { get; set; }
        //Email of the leader 
        [Display(Name = "Email of the leader")]
        [EmailAddress]
        [Required]
        public string LeaderName { get; set; }
        //Id of the leader
        public int LeaderId { get; set; }

    }
}
