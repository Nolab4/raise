﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<Team> teams { get; set; }
        public DbSet<TeamToMembers> teamToMembers { get; set; }
        public DbSet<Todo> todos { get; set; }
        public DbSet<Todo> todosTeam { get; set; }
    }
}
