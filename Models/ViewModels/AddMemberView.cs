﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Models.ViewModels
{
    public class AddMemberVIew
    {
        [EmailAddress]
        [Required]
        public string Email { get; set; }

    }
}
