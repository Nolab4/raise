﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Models.ViewModels
{
    public class ManageView
    {
        public Team _team { get; set; }
        public List<string> _memberEmail { get; set; }
        public ManageView(Team team,List<string> memberEmail)
        {
            _team = team;
            _memberEmail = memberEmail;
        }
    }
}
