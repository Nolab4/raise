﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Models.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name ="First name")] //to display First name instead of FirstName
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress] 
        public string Email { get; set; }
        [Required]
        [StringLength(100,ErrorMessage ="The password must be at least 6 characters long.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [Compare("Password",ErrorMessage ="The password and comfirmation does not match.")]
        [DataType(DataType.Password)]
        [Display(Name ="Confirm password")]
        public string  ConfirmPassword { get; set; }
    }
}
