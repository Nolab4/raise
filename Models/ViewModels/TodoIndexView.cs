﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Models.ViewModels
{
    public class TodoIndexView
    {
        public List<Todo> personal { get; set; }
        public List<Todo> shared { get; set; }
    }
}
