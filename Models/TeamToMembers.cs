﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Models
{
    public class TeamToMembers
    {
        [Key]
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string MemberEmail { get; set; }
    }
}
