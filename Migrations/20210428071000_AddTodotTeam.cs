﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Raise.Migrations
{
    public partial class AddTodotTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "todos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "teamId",
                table: "todos",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "todos");

            migrationBuilder.DropColumn(
                name: "teamId",
                table: "todos");
        }
    }
}
