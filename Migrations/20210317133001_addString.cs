﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Raise.Migrations
{
    public partial class addString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MemberId",
                table: "teamToMembers");

            migrationBuilder.AddColumn<string>(
                name: "MemberEmail",
                table: "teamToMembers",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MemberEmail",
                table: "teamToMembers");

            migrationBuilder.AddColumn<int>(
                name: "MemberId",
                table: "teamToMembers",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
