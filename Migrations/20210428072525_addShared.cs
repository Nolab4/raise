﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Raise.Migrations
{
    public partial class addShared : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_todos",
                table: "todos");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "todos");

            migrationBuilder.DropColumn(
                name: "teamId",
                table: "todos");

            migrationBuilder.RenameTable(
                name: "todos",
                newName: "Todo");

            migrationBuilder.RenameColumn(
                name: "IsDone",
                table: "Todo",
                newName: "IsShared");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Todo",
                table: "Todo",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Todo",
                table: "Todo");

            migrationBuilder.RenameTable(
                name: "Todo",
                newName: "todos");

            migrationBuilder.RenameColumn(
                name: "IsShared",
                table: "todos",
                newName: "IsDone");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "todos",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "teamId",
                table: "todos",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_todos",
                table: "todos",
                column: "Id");
        }
    }
}
