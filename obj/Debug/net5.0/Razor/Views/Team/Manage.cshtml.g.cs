#pragma checksum "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1f37624e51fe46cc9b728cb2bc305627c573710e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Team_Manage), @"mvc.1.0.view", @"/Views/Team/Manage.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\_ViewImports.cshtml"
using Raise;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\_ViewImports.cshtml"
using Raise.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1f37624e51fe46cc9b728cb2bc305627c573710e", @"/Views/Team/Manage.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c680438b54828f74bb4c2d0e1ed8159e61f6d9b6", @"/Views/_ViewImports.cshtml")]
    public class Views_Team_Manage : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Raise.Models.ViewModels.ManageView>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-area", "", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Team", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddMember", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-success"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Todo", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "CreateShared", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-secondary"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "DeleteMember", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger mx-1"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("    <div class=\"d-flex justify-content-md-between\">\r\n        <div class=\"col-8\">\r\n            <h5>Project title: ");
#nullable restore
#line 4 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                          Write(Model._team.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n            <p>Project description: ");
#nullable restore
#line 5 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                               Write(Model._team.ProjectDescription);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n            <div class=\"row pt-4\">\r\n                <div class=\"col-6\">\r\n                    <p>Leader: ");
#nullable restore
#line 8 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                          Write(Model._team.LeaderName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-6\">\r\n            <h5>Members:</h5>\r\n            <div class=\"col-6 w-100\">\r\n");
#nullable restore
#line 15 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                 if (Model._team.LeaderName == User.Identity.Name)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <div class=\"w-100 btn-group\">\r\n                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1f37624e51fe46cc9b728cb2bc305627c573710e7746", async() => {
                WriteLiteral("Add a member");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Area = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1f37624e51fe46cc9b728cb2bc305627c573710e9416", async() => {
                WriteLiteral("Add Todo to a member");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Area = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(" \r\n                </div>\r\n");
#nullable restore
#line 21 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </div>\r\n            <div class=\"col-6\">\r\n");
#nullable restore
#line 24 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                 if (Model._memberEmail.Count > 0)
                {
                    

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                     if (Model._team.LeaderName == User.Identity.Name)
                    {


#line default
#line hidden
#nullable disable
            WriteLiteral(@"                        <table class=""table table-bordered table-striped"">
                            <thead>
                                <tr>
                                    <th width=""20%"">Member</th>
                                    <th width=""10%"">Remove</th>
                                </tr>

                            <tbody>
");
#nullable restore
#line 37 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                 foreach (string email in Model._memberEmail)
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <tr>\r\n");
#nullable restore
#line 40 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                         if (email == User.Identity.Name)
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <td width=\"20%\">");
#nullable restore
#line 42 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                                       Write(email);

#line default
#line hidden
#nullable disable
            WriteLiteral(" (you)</td>\r\n");
#nullable restore
#line 43 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                        }
                                        else
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <td width=\"20%\">");
#nullable restore
#line 46 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                                       Write(email);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </td>\r\n");
#nullable restore
#line 47 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        <td width=\"10%\">\r\n                                            <div class=\"w-100 btn-group\">\r\n                                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1f37624e51fe46cc9b728cb2bc305627c573710e14622", async() => {
                WriteLiteral("Remove");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Area = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_7.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 50 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                                                                                                 WriteLiteral(email);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_8);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                            </div>\r\n                                        </td>\r\n                                    </tr>\r\n");
#nullable restore
#line 54 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                            </tbody>\r\n                        </table>\r\n");
#nullable restore
#line 57 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                    }
                    else
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                        <table class=""table table-bordered table-striped"">
                            <thead>
                                <tr>
                                    <th width=""20%"">Member</th>
                                </tr>

                            <tbody>
");
#nullable restore
#line 67 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                 foreach (string email in Model._memberEmail)
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <tr>\r\n");
#nullable restore
#line 70 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                         if (email == User.Identity.Name)
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <td width=\"20%\">");
#nullable restore
#line 72 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                                       Write(email);

#line default
#line hidden
#nullable disable
            WriteLiteral(" (you)</td>\r\n");
#nullable restore
#line 73 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                        }
                                        else
                                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                            <td width=\"20%\">");
#nullable restore
#line 76 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                                       Write(email);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </td>\r\n");
#nullable restore
#line 77 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    </tr>\r\n");
#nullable restore
#line 79 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                                }

#line default
#line hidden
#nullable disable
            WriteLiteral("                            </tbody>\r\n                        </table>\r\n");
#nullable restore
#line 82 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 82 "C:\Users\tibo4\Desktop\Courses\SPE\S4\Courses\Software engering\sofware-engineering\Views\Team\Manage.cshtml"
                     
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </div>\r\n        </div>\r\n    </div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Raise.Models.ViewModels.ManageView> Html { get; private set; }
    }
}
#pragma warning restore 1591
