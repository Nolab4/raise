﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Raise.Models;
using Raise.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Controllers
{
    public class TeamController : Controller
    {
        private readonly ApplicationDbContext _db;
        public TeamController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Team model)
        {
            if(ModelState.IsValid)
            {
                Team team = new Team
                {
                    Name = model.Name,
                    ProjectDescription = model.ProjectDescription,
                    LeaderName = model.LeaderName,
                };
                _db.teams.Add(team);
                _db.SaveChanges();
                TeamToMembers teamToLeader = new TeamToMembers
                {
                    TeamId = team.Id,
                    MemberEmail = model.LeaderName
                };
                _db.teamToMembers.Add(teamToLeader); //add relationbetween leader and team
                _db.SaveChanges();
                return RedirectToAction("Index", "Team");
            }
            //in case of error return to home page
            return RedirectToAction("Index", "Home");
            
        }
        //display all the teams linked to the user
        public IActionResult Index()
        {
            List<TeamToMembers> object_list = _db.teamToMembers.ToList();
            List<Team> teams = _db.teams.ToList();
            List<Team> res = new List<Team>();
            foreach(TeamToMembers t in object_list)
            {
                if(t.MemberEmail == User.Identity.Name)
                {
                    res.Add(_db.teams.Find(t.TeamId));
                }
            }
            return View(res);
        }

        //manage the team
        public IActionResult Manage(int? id)
        {
            if(id == null || id == 0)
            {
                return NotFound();
            }
            Team team = _db.teams.Find(id);
            if(team == null)
            {
                return NotFound();
            }
            List<TeamToMembers> teamToMembers = _db.teamToMembers.ToList();
            List<string> membersEmail = new List<string>();
            foreach(TeamToMembers t in teamToMembers)
            {
                if (t.TeamId == team.Id)
                    membersEmail.Add(t.MemberEmail);
            }
            ManageView mView = new ManageView(team,membersEmail);
            //TODO: to display team members, add a list of users in class team
            return View(mView);
        }

        public IActionResult AddMember()
        { 
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddMember(AddMemberVIew model)
        {
            List<Team> teams = _db.teams.ToList();
            Team currentTeam = new Team();
            bool stop = false;
            for(int i = 0; i< teams.Count && !stop;i++) //get the current team
            {
                if (teams[i].LeaderName == User.Identity.Name)
                {
                    currentTeam = teams[i];
                    stop = true;
                }
            }
            //TODO: check if user exists
            TeamToMembers teamToMember = new TeamToMembers
            {
                TeamId = currentTeam.Id,
                MemberEmail = model.Email
            };
            _db.teamToMembers.Add(teamToMember);
            _db.SaveChanges();
            return RedirectToAction("Index", "Team");
        }
    }
}
