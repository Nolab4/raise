﻿using Microsoft.AspNetCore.Mvc;
using Raise.Models;
using Raise.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Raise.Controllers
{
    public class TodoController : Controller
    {
        private readonly ApplicationDbContext _db;
        public TodoController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            List<Todo> todos = _db.todosTeam.ToList();
            TodoIndexView view = new TodoIndexView();
            view.shared = new List<Todo>();
            view.personal = new List<Todo>();
            for(int i = 0; i < todos.Count;i++)
            {
                if (todos[i].User == User.Identity.Name && todos[i].IsShared)
                    view.shared.Add(todos[i]);
                if (todos[i].User == User.Identity.Name && !todos[i].IsShared)
                    view.personal.Add(todos[i]);
            }
            return View(view);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Todo model)
        {
            if(ModelState.IsValid)
            {
                Todo todo = new Todo
                {
                    Title = model.Title,
                    Details = model.Details,
                    User = User.Identity.Name,
                    IsShared = false
                };
                _db.todosTeam.Add(todo);
                _db.SaveChanges();
                return RedirectToAction("Index", "Todo");
            }
            return RedirectToAction("Index", "Home"); //return home in case of error
        }
        
        public IActionResult CreateShared()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateShared(Todo model)
        {
            if(ModelState.IsValid)
            {
                Todo todo = new Todo
                {
                    Title = model.Title,
                    Details = model.Details,
                    User = model.User,
                    IsShared = true
                };
                _db.todosTeam.Add(todo);
                _db.SaveChanges();
                return RedirectToAction("Index", "Team");
            }
            return RedirectToAction("Index", "Home");
        }
        
        public IActionResult Modify(int? Id)
        {
            if (Id == null || Id == 0)
            {
                return NotFound();
            }
            Todo todo = _db.todos.Find(Id);
            if (todo == null)
            {
                return NotFound();
            }
            return View(todo);
        }
        //Post Update
        [HttpPost]
        [ValidateAntiForgeryToken] //Check if log in
        public IActionResult Modify(Todo todo)
        {
            if (ModelState.IsValid)
            {
                todo.User = User.Identity.Name;
                _db.todosTeam.Update(todo);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(todo);
        }

        public IActionResult Finish(int? Id)
        {
            if (Id == null || Id == 0)
            {
                return NotFound();
            }
            _db.todosTeam.Remove(_db.todosTeam.Find(Id)); //remove the corresponding todo
            _db.SaveChanges();
            return RedirectToAction("Index"); //IDEA -> save todo done and display it in other part
        }
    }
}
